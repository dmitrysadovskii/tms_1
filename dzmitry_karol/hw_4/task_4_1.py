# Перевести строку в массив
# "Robin Singh" => ["Robin”, “Singh"]
# "I love arrays they are my favorite"
# => ["I", "love", "arrays", "they", "are", "my", "favorite"]

my_str = 'My new test string'
print(my_str.split())
