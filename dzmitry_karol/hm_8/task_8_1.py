def typed(typ):
    def decorator(add):
        def wrapper(*args):
            if typ == 'str':
                print(''.join([str(i) for i in args]))
            else:
                print(sum([float(i) for i in args]))

        return wrapper

    return decorator


@typed(typ='str')
def add(a, b):
    return a + b


add("3", 5)  # -> "35"
add(5, 5)  # -> "55"
add('a', 'b')  # -> 'ab’


@typed(typ='int')
def add(a, b, c):
    return a + b + c


add(5, 6, 7)  # -> 18
add("3", 5)  # -> 8
add(0.1, 0.2, 0.4)  # -> 0.7000000000000001
