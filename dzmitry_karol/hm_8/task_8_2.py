number_names = {
    0: 'zero', 1: 'one', 2: 'two', 3: 'three',
    4: 'four', 5: 'five', 6: 'six', 7: 'seven', 8: 'eight',
    9: 'nine', 10: 'ten', 11: 'eleven', 12: 'twelve',
    13: 'thirteen', 14: 'fourteen', 15: 'fifteen', 16: 'sixteen',
    17: 'seventeen', 18: 'eighteen', 19: 'nineteen'}


def decorator(func):
    def wrapper(arr):
        new_arr = []
        arr = [int(i) for i in arr.split()]
        for key, value in number_names.items():
            for i in arr:
                if i == key:
                    new_arr.append(value)
        func(' '.join(sorted(new_arr)))

    return wrapper


@decorator
def func_sort(arr):
    print(arr)


func_sort('1 2 3')
func_sort('1 2 5')
