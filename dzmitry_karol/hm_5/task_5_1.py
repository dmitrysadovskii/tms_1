import random

num = [x for x in range(10)]
random.shuffle(num)
num = num[:4]
if num[0] == 0:
    num[0] = num[1]
    num[1] = 0
print(num)

while True:
    answer = input('Введите 4-х значное число:')
    bull = 0
    cow = 0
    if len(answer) == 0:
        print('Число должно быть из 4-х цифр!')
        continue
    if not 999 < int(answer) < 10000:
        print('Число должно быть из 4-х цифр!')
        continue
    test = [int(x) for x in answer]
    if len(test) != len(set(test)):
        print('Цифры в числе не должны повторяться!')
        continue

    if test == num:
        print('Победа!')
        break
    for a, b in enumerate(test):
        if num[a] == b:
            bull += 1
        elif b in num:
            cow += 1
    print(f'Быков: {bull}; Коров: {cow}')
