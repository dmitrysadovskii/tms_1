list_1 = [1, 5, 2, 4, 3]
list_2 = [1, 2, 3, 4, 5]


def check_numbers(arr):
    """v:1.0"""
    i = 0
    result = []
    while i < len(arr) - 1:
        if arr[i] < arr[i + 1]:
            result.append(arr[i + 1])
        i += 1
    print(result)


check_numbers(list_1)
check_numbers(list_2)


def check_numbers1(arr):
    """v:2.0"""
    res = [arr[i] for i in range(1, len(arr)) if arr[i] > arr[i - 1]]
    return res


print(check_numbers1(list_1))
print(check_numbers1(list_2))
