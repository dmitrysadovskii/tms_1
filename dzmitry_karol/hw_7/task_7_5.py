text_1 = 'hello world'
text_2 = 'This is a test string! It is test!'


def encode(txt: str, n: int):
    abc = 'abcdefghijklmnopqrstuvwxyz'
    code = ''
    for letter in txt:
        if letter in abc:
            num = abc.find(letter)
            new_letter = (num + n)
            code += abc[new_letter]
        else:
            code += letter
    return code


def decode(txt: str, n: int):
    abc = 'abcdefghijklmnopqrstuvwxyz'
    code = ''
    for letter in txt:
        if letter in abc:
            num = abc.find(letter)
            new_letter = (num - n)
            code += abc[new_letter]
        else:
            code += letter
    return code


print(encode(text_2, 2))
print(decode(encode(text_2, 2), 2))
