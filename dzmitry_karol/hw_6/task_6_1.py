def validate(card):
    sum = 0
    num = len(card)
    oddeven = num & 1
    for count in range(0, num):
        digit = int(card[count])
        if not ((count & 1) ^ oddeven):
            digit = digit * 2
        if digit > 9:
            digit = digit - 9
        sum = sum + digit
    if sum % 10 == 0:
        print("Card is valid!")
    else:
        print("Card is invalid!")


while True:
    card = input('Введите номер карты:')
    # card = '4561261212345467'
    if len(card) == 0:
        print('Необходимо ввести число.')
        continue
    elif card.isdigit() is False:
        print('Введенный текст должен быть числом.')
    else:
        validate(card)
        break
