import mysql.connector as mysql

db = mysql.connect(host="localhost",
                   user="root",
                   passwd="!23QWEasd",
                   database='test_db')
cursor = db.cursor()
# query = """CREATE TABLE orders (
#     order_no VARCHAR(255),
#     purch_amt VARCHAR(255),
#     ord_date VARCHAR(255),
#     customer_id VARCHAR(255),
#     salesman_id VARCHAR(255)
#     )"""
# cursor.execute(query)
# db.commit()


# query = """INSERT INTO orders (
#         order_no,
#         purch_amt,
#         ord_date,
#         customer_id,
#         salesman_id)
#         VALUES (%s, %s, %s, %s, %s)"""
# values = [
#     ('70001', '150.5', '2012-10-05', '3005', '5002'),
#     ('70009', '270.65', '2012-09-10', '3001', '5005'),
#     ('70002', '65.26', '2012-10-05', '3002', '5001'),
#     ('70004', '110.5', '2012-08-17', '3009', '5003'),
#     ('70007', '948.5', '2012-09-10', '3005', '5002'),
#     ('70005', '2400.6', '2012-07-27', '3007', '5001'),
#     ('70008', '5760', '2012-09-10', '3002', '5001'),
#     ('70010', '1983.43', '2012-10-10', '3004', '5006'),
#     ('70003', '2480.4', '2012-10-10', '3009', '5003'),
#     ('70012', '250.45', '2012-06-27', '3008', '5002'),
#     ('70011', '75.29 ', '2012-08-17', '3003', '5007'),
#     ('70013', '3045.6', '2012-04-25', '3002', '5001')
# ]
# cursor.executemany(query, values)
# db.commit()


query = ["select order_no, ord_date, purch_amt "
         "from orders where salesman_id ='5002'",
         "select distinct salesman_id from orders",
         "select ord_date, salesman_id, order_no,  purch_amt  from orders",
         "select * from orders where order_no between '70001' and '70007'"
         ]
for i in query:
    cursor.execute(i)
    print(cursor.fetchall())

db.close()
