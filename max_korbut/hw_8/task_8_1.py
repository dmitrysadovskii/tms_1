"""декораторы"""


def typed(type_operation):

    def decorator(func):

        def wrapper(*args):
            if type_operation == 'str':
                return sum_str(*args)
            elif type_operation == 'int':
                return sum_int(*args)

        def sum_str(*args):
            if len(args) == 2:
                a = str(args[0])
                b = str(args[1])
                return func(a, b)
            elif len(args) == 3:
                a = str(args[0])
                b = str(args[1])
                c = str(args[2])
                return func(a, b, c)

        def sum_int(*args):
            if len(args) == 2:
                a = int(args[0])
                b = int(args[1])
                return func(a, b)
            elif len(args) == 3:
                a = int(args[0])
                b = int(args[1])
                c = int(args[2])
                return func(a, b, c)

        return wrapper
    return decorator


@typed(type_operation='str')
def add_str(a, b):
    return a + b


print(add_str('3', 5))
print(add_str(5, 5))
print(add_str('a', 'b'))


@typed(type_operation='int')
def add_int(a, b, с):
    return a + b + с


print(add_int(5, 6, 7))
print(add_int(0.1, 0.2, 0.4))
