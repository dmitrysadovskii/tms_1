"""Калькулятор v0.2"""


def decorator(func):

    def wrapper():
        operations = {
            '+': 1,
            '-': 2,
            '*': 3,
            '/': 4,
        }
        type_operation = 0
        question = int(input("Вы хотите:\n1.Ввести операцию сразу "
                             "\n2.Вводить все поочередно \nУкажите цифру: "))
        if question == 2:
            func()
        elif question == 1:
            operation = str(input("Введите выражение через пробел: "))
            operation = operation.split()
            for symbol in operation:
                for key, value in operations.items():
                    if symbol == key:
                        type_operation = value
            first_number = int(operation[0])
            second_number = int(operation[-1])
            calculator(first_number, second_number, type_operation)

    return wrapper


def calculator_numbers():
    """Запрашивает у пользователя тип операции и числа"""

    while True:
        print("1. Сложение\n2. Вычитание\n3. Умножение\n4. Деление")
        type_operation = int(input("Введите номер операции без точки: "))
        if type_operation == 1:
            print("Вы выбрали оперциию 'Сложение'!")
        elif type_operation == 2:
            print("Вы выбрали операцию 'Вычитание'!")
        elif type_operation == 3:
            print("Вы выбрали оперцию 'Умножение'!")
        elif type_operation == 4:
            print("Вы выбрали операцию 'Деление'!")
        else:
            print("Вы выбрали несуществующую операцию!")
            break
        first_number = int(input("Введите первое число: "))
        second_number = int(input("Введите второе число: "))
        if second_number == 0 and type_operation == 4:
            print("На 0 делить нельзя!")
            break
        calculator(first_number, second_number, type_operation)
        break


def calculator(first_number, second_number, type_operation):
    """Производит выбранную операцию и выводит результат"""

    result = 0
    if type_operation == 1:
        result = first_number + second_number
    elif type_operation == 2:
        result = first_number - second_number
    elif type_operation == 3:
        result = first_number * second_number
    elif type_operation == 4:
        result = first_number / second_number
    print(f"Результат вашей операции - '{result}'")


@decorator
def cycle():
    """Запускает всю программу"""

    while True:
        calculator_numbers()
        print("Хоте продолжить?")
        question = input("Да/Нет: ")
        if question.title() == 'Да':
            continue
        elif question.title() == 'Нет':
            break


if __name__ == '__main__':
    cycle()
