import mysql.connector as mysql


def connect_to_db():
    db_connect = mysql.connect(
        host="localhost",
        user="root",
        passwd="1111",
        database="test_db")
    return db_connect


def create_orders_table_in_db(db_connect):
    cursor = db_connect.cursor()
    cursor.execute("""CREATE TABLE orders (ord_no INT(9), purch_amt FLOAT(9),
    ord_date DATETIME NOT NULL, customer_id INT(9), salesman_id INT(9))""")


def add_data_to_table(db_connect, values):
    cursor = db_connect.cursor()
    query = """INSERT INTO orders (ord_no, purch_amt, ord_date, customer_id,
    salesman_id) VALUES (%s, %s, %s, %s, %s)"""
    cursor.executemany(query, values)
    db_connect.commit()
    print(cursor.rowcount, "record inserted")


def task_1(db_connect):
    """номер заказа, дата, и кол-во для каждого товара у продовца 5002"""

    cursor = db_connect.cursor()
    query = """SELECT ord_no, ord_date, purch_amt FROM orders WHERE
    salesman_id=5002"""
    cursor.execute(query)
    print(cursor.fetchall())


def task_2(db_connect):
    """уникальные id продавца"""

    cursor = db_connect.cursor()
    query = """SELECT DISTINCT salesman_id FROM orders"""
    cursor.execute(query)
    print(cursor.fetchall())


def task_3(db_connect):
    """по порядку дату, id, номер заказа и кол-во"""

    cursor = db_connect.cursor()
    query = """SELECT ord_date, salesman_id, ord_no, purch_amt FROM orders
    ORDER BY ord_date, salesman_id, ord_no, purch_amt"""
    cursor.execute(query)
    print(cursor.fetchall())


def task_4(db_connect):
    """заказы между 70001 и 70007"""

    cursor = db_connect.cursor()
    query = """SELECT * FROM orders WHERE ord_no BETWEEN 70001 AND 70007"""
    cursor.execute(query)
    print(cursor.fetchall())


if "__main__" == __name__:
    data = [
        (70001, 150.5, '2012-10-05', 3005, 5002),
        (70009, 270.65, '2012-09-10', 3001, 5005),
        (70002, 65.26, '2012-10-05', 3002, 5001),
        (70004, 110.5, '2012-08-17', 3009, 5003),
        (70007, 948.5, '2012-09-10', 3005, 5002),
        (70005, 2400.6, '2012-07-27', 3007, 5001),
        (70008, 5760, '2012-09-10', 3002, 5001),
        (70010, 1983.43, '2012-10-10', 3004, 5006),
        (70003, 2480.4, '2012-10-10', 3009, 5003),
        (70012, 250.45, '2012-06-27', 3008, 5002),
        (70011, 75.29, '2012-08-17', 3003, 5007),
        (70013, 3045.6, '2012-04-25', 3002, 5001)
    ]
    connect = connect_to_db()
    create_orders_table_in_db(connect)
    add_data_to_table(connect, data)
    task_1(connect)
    task_2(connect)
    task_3(connect)
    task_4(connect)
