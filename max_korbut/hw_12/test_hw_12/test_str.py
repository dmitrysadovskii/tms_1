import unittest
from .count_str import count_letters


class TestStrPositive(unittest.TestCase):

    def setUp(self):
        print('Start')

    def tearDown(self):
        print('End')

    def test_positive_1(self):
        print('Positive 1')
        self.assertEqual(count_letters('cccbba'), 'c3b2a')

    def test_positive_2(self):
        print('Positive 2')
        self.assertEqual(count_letters('abeehhhhhccced'), 'abe3h5c3d')

    def test_positive_3(self):
        print('Positive 3')
        self.assertEqual(count_letters('аабббвввв'), 'а2б3в4')

    def test_positive_4(self):
        print('Positive 4')
        self.assertEqual(count_letters('abcde'), 'abcde')

    def test_positive_5(self):
        print('Positive 5')
        self.assertEqual(count_letters('ABBCCCDDDD'), 'AB2C3D4')

    def test_positive_6(self):
        print('Positive 6')
        self.assertEqual(count_letters('aaaaaaaaaaaa'), 'a12')


@unittest.expectedFailure
class TestStrNegative(unittest.TestCase):

    def setUp(self):
        print('Start')

    def tearDown(self):
        print('End')

    def test_negative_1(self):
        print('Negative 1')
        self.assertEqual(count_letters('1231'), 'Проверьте правильность ввода')

    def test_negative_2(self):
        print('Negative 2')
        self.assertEqual(count_letters(''), 'Проверьте правильность ввода')

    def test_negative_3(self):
        print('Negative 3')
        self.assertEqual(count_letters('/.,?'), 'Проверьте правильность ввода')

    def test_negative_4(self):
        print('Negative 4')
        self.assertEqual(count_letters('a1b2'), 'Проверьте правильность ввода')

    def test_negative_5(self):
        print('Negative 5')
        self.assertEqual(count_letters('ab c'), 'Проверьте правильность ввода')
