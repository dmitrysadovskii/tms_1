def test_login(browser):
    browser.get('http://thedemosite.co.uk/login.php')
    user_name = 'Maks'
    passwd = '1111'
    username_field = browser.find_element_by_name('username')
    username_field.send_keys(user_name)
    password_field = browser.find_element_by_name('password')
    password_field.send_keys(passwd)
    login_button = browser.find_element_by_name('FormsButton2')
    login_button.click()
    status_check = browser.find_element_by_xpath("//big//b")
    assert status_check.text == "**Successful Login**"
