import pytest
import random


@pytest.fixture(scope='class')
def fixture_class_start():
    print('Start class testing')
    yield
    print('End class testing')


@pytest.fixture()
def fixture_start_test():
    print('Start test')
    yield
    print('End test')


@pytest.fixture()
def random_n():
    number = random.sample('0123456789', 4)
    return ''.join(number)
