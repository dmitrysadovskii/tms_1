"""деньги кредит банки"""


class Contribution:
    """Создание вклада"""

    def __init__(self, sum_deposit, term):
        self.sum_deposit = sum_deposit
        self.term = term


class Bank(Contribution):
    """Расчет капитализации"""

    def __init__(self, sum_deposit, term):
        super().__init__(sum_deposit, term)

    def deposit(self):
        new_sum_deposit = self.sum_deposit
        term = self.term * 12
        percents = 25
        percents /= 100

        for _ in range(0, term):
            capitalization = new_sum_deposit * percents
            capitalization /= 12
            new_sum_deposit += capitalization

        print(new_sum_deposit)


dep_1 = Bank(1000, 1)
dep_1.deposit()

dep_2 = Bank(1000, 10)
dep_2.deposit()
