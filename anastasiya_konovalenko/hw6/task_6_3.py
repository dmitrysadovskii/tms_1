print('Выберите операцию:', '1. Сложение', '2. Вычитание',
      '3. Деление', '4. Умножение')
s = int(input())
print('Первое число = ')
a = int(input())
print('Второе число = ')
b = int(input())


def func(a, b, s):
    if s == 1:
        result = a + b
    elif s == 2:
        result = a - b
    elif s == 3:
        assert b != 0, 'Division by zero'
        result = f"Частное = {a // b}; Остаток = {a % b}"
    elif s == 4:
        result = a * b
    return result


print(f'Ответ: {func(a, b, s)}')
