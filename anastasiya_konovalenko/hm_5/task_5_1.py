import random


def gen_answer():
    n = [i for i in range(10)]
    number = []
    for elem in range(4):
        a = n.pop(random.choice(range(len(n))))
        number.append(str(a))
    return ''.join(number)


def bulls_cows(num, answer):
    bulls, cows = 0, 0
    for i in range(4):
        if str(num)[i] == str(answer)[i]:
            bulls += 1
        elif num[i] in str(answer):
            cows += 1
    return [bulls, cows]


answer_ = gen_answer()


def game():
    bulls = 0
    while bulls < 4:
        user_answer = input('Четырёхзначное число = ')
        bulls, cows = bulls_cows(user_answer, answer_)
        print(cows, 'cows', bulls, 'bulls')
    print('Выигрыш')


print(game())
