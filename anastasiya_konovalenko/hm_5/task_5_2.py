names = ['Ann', 'Max', 'Kate', 'Mark', 'Martha']

elem_max = max([elem for elem in range(len(names))]) + 1
i = elem_max - 2

if elem_max == 3:
    print(names[0] + ', ' + names[1] + ' and ' + names[2] + ' like this')
elif elem_max == 1:
    print(names[0], 'likes this')
elif elem_max == 0:
    print('no one likes this')
elif elem_max > 3:
    print(names[0] + ', ' + names[1] + f' and {i} others likes this')
