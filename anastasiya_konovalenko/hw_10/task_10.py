from abc import ABC, abstractmethod


class Flower(ABC):

    @abstractmethod
    def __init__(self, fl_name: str, fl_price: int,
                 fl_length: int, fl_lifetime: int):
        self.fl_name = fl_name
        self.fl_price = fl_price
        self.fl_length = fl_length
        self.fl_lifetime = fl_lifetime


class Lily(Flower):
    def __init__(self):
        super().__init__('lily', 19, 8, 2)


class Orchid(Flower):
    def __init__(self):
        super().__init__('orchid', 16, 6, 5)


class Lotus(Flower):
    def __init__(self):
        super().__init__('lotus', 24, 7, 4)


class Bouquet:
    flowers = []

    def add_flower(self, flower: Flower):
        self.flowers.append(flower)

    def cost(self):
        cost = 0
        for flower in self.flowers:
            cost += flower.fl_price
        return cost

    def lifetime(self):
        avg_lft = 0
        for flower in self.flowers:
            avg_lft += flower.fl_lifetime

        if len(self.flowers) > 0:
            return avg_lft / len(self.flowers)
        else:
            return 0

    def show_flowers(self):
        for flower in self.flowers:
            print('name: ' + flower.fl_name + ', price: ' + str(
                  flower.fl_price) + ', length: ' + str(
                  flower.fl_length) + ', lifetime: ' + str(flower.fl_lifetime))

    def sort(self, field):
        self.flowers.sort(key=lambda x: getattr(x, field))

    def find_flower(self, key, value):
        result = []
        for i in self.flowers:
            if getattr(i, key) == value:
                result.append(i)
        for flower in result:
            print('name: ' + flower.fl_name)


lily1 = Lily()
lily2 = Lily()
orchid = Orchid()
lotus1 = Lotus()
lotus2 = Lotus()

bouquet = Bouquet()

bouquet.add_flower(lily1)
bouquet.add_flower(lily2)
bouquet.add_flower(orchid)
bouquet.add_flower(lotus1)
bouquet.add_flower(lotus2)


bouquet.sort('fl_length')
bouquet.show_flowers()

bouquet.find_flower('fl_length', 6)
bouquet.find_flower('fl_name', 'lily')
