import yaml
import json


with open('order.yaml') as f:
    templates = yaml.safe_load(f)

print('Номер заказа -', templates['invoice'])
for key, value in templates['bill-to']['address'].items():
    print(key, '-', value)

product = templates['product']
for elem in product:
    for key, value in elem.items():
        print(key, '-', value)

with open('order.yaml') as f_yaml, open('convertation.json', 'w') as f_json:
    yaml_file = yaml.safe_load(f_yaml)
    json.dump(str(yaml_file), f_json)
