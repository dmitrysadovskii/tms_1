def time(hour, minute):
    if hour in range(1, 12):
        # Время с часу ночи до 11 утра
        print(hour, ':', minute, ' am', sep='')
    elif hour in range(13, 25):
        # Время с 13 часов дня до полночи
        print(hour - 12, ':', minute, ' pm', sep='')
    elif hour == 0:
        # Попытка избежать возможных ошибок с пониманием полночи
        print(hour + 12, ':', minute, ' am', sep='')
    else:
        print(hour, ':', minute, ' pm', sep='')
    return time


time(12, 12)
# Ввод не предполагает наличие нуля перед часом
