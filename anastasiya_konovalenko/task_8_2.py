def elem_in_alphabet_order(func):

    def wrapped(elements):
        number_names = {0: 'zero', 1: 'one', 2: 'two', 3: 'three', 4: 'four',
                        5: 'five', 6: 'six', 7: 'seven', 8: 'eight', 9: 'nine',
                        10: 'ten', 11: 'eleven', 12: 'twelve', 13: 'thirteen',
                        14: 'fourteen', 15: 'fifteen', 16: 'sixteen',
                        17: 'seventeen', 18: 'eighteen', 19: 'nineteen'}
        list_of_num = elements.split()
        list_values = []
        for elem in list_of_num:
            list_values.append(number_names.get(int(elem)))
        list_values.sort()
        final_list = []
        for el in list_values:
            for key, val in number_names.items():
                if val == el:
                    final_list.append(key)
        return func(final_list)
    return wrapped


@elem_in_alphabet_order
def numbers(elem):
    return elem


print(numbers('1 5 7 6'))
