def type_str(*args):
    comp_lst = [elem for elem in args]
    convert = list(map(lambda x: str(x), comp_lst))
    return convert


def type_int(*args):
    comp_lst = [elem for elem in args]
    convert = list(map(lambda x: int(x), comp_lst))
    return convert


def typed(types):

    def dec(func):
        def wrapped(*args):
            if types == 'str':
                return func(''.join(type_str(*args)))
            elif types == 'int':
                return func(sum(type_int(*args)))
        return wrapped
    return dec


@typed(types='str')
def add(*args):
    return args[0]


print(add(5, '2', '5'))
