from pages.main_page import MainPage
from pages.contact_us_page import ContactUsPage


def test_contact_us_form(browser):
    main_page = MainPage(browser)
    main_page.open_base_page()
    main_page.open_contact_page()
    contact_page = ContactUsPage(browser)
    contact_page.should_be_contact_page()
    contact_page.contact_form_is_present()
    contact_page.contact()
    contact_page.message_successfully_sent()
