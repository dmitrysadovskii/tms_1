from selenium.webdriver.common.by import By


class SectionsShirts:

    LOCATOR_SECTIONS_T_SHIRTS = (By.XPATH,
                                 "//div[@class='sf-contener clearfix col-lg-12"
                                 "']//ul[@class='sf-menu clearfix menu-content"
                                 " sf-js-enabled sf-arrows']/li/a[text()='T-sh"
                                 "irts']")
    LOCATOR_PRODUCT_SHIRTS = (By.CSS_SELECTOR, ".product_img_link > .replace-2"
                                               "x.img-responsive")
    LOCATOR_OPEN_WINDOW_PRODUCT = (By.CSS_SELECTOR, '.ajax_block_product.col-x'
                                                    's-12.col-sm-6.col-md-4.fi'
                                                    'rst-in-line.last-line.fir'
                                                    'st-item-of-tablet-line.fi'
                                                    'rst-item-of-mobile-line.l'
                                                    'ast-mobile-line')
    LOCATOR_ADD_SHIRT_IN_CART = (By.XPATH,
                                 "//span[text()='Add to cart']")
    LOCATOR_CONTINUE_IN_SECTIONS_SHIRTS = (By.CSS_SELECTOR, '.cross')
