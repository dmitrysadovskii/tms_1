from selenium.webdriver.common.by import By


class CartLocator:

    LOCATOR_IN_CART = (By.XPATH, "//b[text() = 'Cart']")
    LOCATOR_PRODUCT = (By.ID, 'product_1_1_0_450159')
