number_names = {
    0: 'zero', 1: 'one', 2: 'two', 3: 'three', 4: 'four',
    5: 'five', 6: 'six', 7: 'seven', 8: 'eight', 9: 'nine',
    10: 'ten', 11: 'eleven', 12: 'twelve', 13: 'thirteen',
    14: 'fourteen', 15: 'fifteen', 16: 'sixteen', 17: 'seventeen',
    18: 'eighteen', 19: 'nineteen'}

names_number = {
    'zero': 0, 'one': 1, 'two': 2, 'three': 3, 'four': 4, 'five': 5,
    'six': 6, 'seven': 7, 'eight': 8, 'nine': 9, 'ten': 10,
    'eleven': 11, 'twelve': 12, 'thirteen': 13, 'fourteen': 14,
    'fifteen': 15, 'sixteen': 16, 'seventeen': 17, 'eighteen': 18,
    'nineteen': 19}


num_input = list(map(int, input('Введите числа от 0 до 20 через пробел'
                                ' для вывода в порядке лексикографического'
                                ' возрастания: ').split()))


if len(num_input) > 100:
    print('Слишком много значений!')

for i in num_input:
    if i < 0 or i > 20:
        print('Значение выходит из диапазона 0 - 20 ')


def func(arg):
    a = []
    for i in arg:
        a.append(number_names[int(i)])
        if i not in arg:
            print('jswgnseg')
    a = sorted(a)
    b = []
    for i in a:
        b.append(names_number[i])
    for i in b:
        print(i, end=' ')


func(num_input)
