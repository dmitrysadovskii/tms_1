print('Шифр цезаря')
move = int(input('Введите параметр сдвига (1-25): '))
text = input('Ведите строку которую необходимо зашифровать: ')
alfabet_lat = 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz'


def encode(move, text):
    text_output = ''
    for element in text:
        index = alfabet_lat.find(element)
        index_new = index + move
        if element in alfabet_lat:
            text_output += alfabet_lat[index_new]
        else:
            text_output += element
    return text_output


text_encode = encode(move, text)


def decode(move, text_encode):
    text_output_decode = ''
    for element in text_encode:
        index_1 = alfabet_lat.find(element)
        index_2 = index_1 - move
        if element in alfabet_lat:
            text_output_decode += alfabet_lat[index_2]
        else:
            text_output_decode += element
    return text_output_decode


print(encode(move, text))
print(decode(move, text_encode))
