# Работа с yaml файлом
# Напечатайте номер заказа
# Напечатайте адрес отправки
# Напечатайте описание посылки, ее стоимость и кол-во
# Сконвертируйте yaml файл в json
# Создайте свой yaml файл

import yaml
import json

# Чтение и принты файла Yaml
with open('order.yaml', 'r') as f:
    file_yaml = yaml.safe_load(f)

print('Invoice: ', file_yaml['invoice'])
print('Address:')
for k, v in file_yaml['bill-to']['address'].items():
    if k == 'lines':
        pass
    else:
        print(k, v, end=' ')
print(file_yaml['bill-to']['address']['lines'], end='')
print('Description: ', file_yaml['product'][0]['description'])
print('Price: ', file_yaml['product'][0]['price'])
print('Qauntity: ', file_yaml['product'][0]['quantity'])
print('Description: ', file_yaml['product'][1]['description'])
print('Price: ', file_yaml['product'][1]['price'])
print('Qauntity: ', file_yaml['product'][1]['quantity'])

# Произвольный словарь
if __name__ == '__main__':
    data = {'a1': [1, 2, 3, 4],
            'a2': 'example_value',
            'a3': {'b1': ['a', 'b', 'c', 'd'], 'b2': 'value2', 'b3': 3}
            }

# Создание YAML файла
# with open('example.yaml', 'w', encoding='utf8') as f:
#     yaml.dump(data, f)

# Конвертация YAML -> Json
with open('example.yaml', 'r') as f:
    data_yaml = yaml.safe_load(f)

with open('example.json', 'w') as f:
    json.dump(data_yaml, f)
