# Разработайте поиск книги в библиотеке по ее автору(часть имени)
# /цене/заголовку/описанию.

import xml.etree.ElementTree as ET

tree = ET.parse('library.xml')
root = tree.getroot()


def search_book(xml_files):
    search = str(input('Search: '))
    counter = 0
    for elem in xml_files:
        for i in elem:
            if i.text[0] == search[0] and i.text[1] == search[1]\
                    and i.text[2] == search[2]:
                print(f'Возможно эта книга: {i.text} {elem.attrib}')
                counter += 1

    if counter == 0:
        print('Nothing found')


search_book(root)
