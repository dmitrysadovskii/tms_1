
def encode(set: str, n: int):
    alf_en = 'abcdefghijklmnopqrstuvwxyz'
    alf_v_en = alf_en.upper()
    alf_ru = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
    alf_v_ru = alf_ru.upper()
    en = ''

    for i in set:
        if i in alf_v_en:
            ret = alf_v_en.index(i)
            fit = ret + n
            en += alf_v_en[fit]
        elif i in alf_en:
            ret = alf_en.index(i)
            fit = ret + n
            en += alf_en[fit]
        elif i in alf_v_ru:
            ret = alf_v_ru.index(i)
            fit = ret + n
            en += alf_v_ru[fit]
        elif i in alf_ru:
            ret = alf_ru.index(i)
            fit = ret + n
            en += alf_ru[fit]
        else:
            en += i
    return en


print(encode('hello world!', 3))
print(encode('this is a test string', 5))
print(encode('test STRing_Ввод Цифр 123', 2))


def decode(tes: str, m: int):
    alf_en = 'abcdefghijklmnopqrstuvwxyz'
    alf_v_en = alf_en.upper()
    alf_ru = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
    alf_v_ru = alf_ru.upper()
    en = ''

    for i in tes:
        if i in alf_v_en:
            ret = alf_v_en.index(i)
            fit = ret - m
            en += alf_v_en[fit]
        elif i in alf_en:
            ret = alf_en.index(i)
            fit = ret - m
            en += alf_en[fit]
        elif i in alf_v_ru:
            ret = alf_v_ru.index(i)
            fit = ret - m
            en += alf_v_ru[fit]
        elif i in alf_ru:
            ret = alf_ru.index(i)
            fit = ret - m
            en += alf_ru[fit]
        else:
            en += i
    return en


print(decode('khoor zruog!', 3))
print(decode('ymnx nx f yjxy xywnsl', 5))
print(decode('vguv UVTkpi_Ддрё Шкцт 123', 2))
