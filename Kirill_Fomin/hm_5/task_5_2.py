names = ["Ann", "Alex", "Mark", "Max"]
likes = len(names)

if likes < 1:
    print("no one likes this")
elif likes == 1:
    print(f"{names[0]} likes this")
elif likes == 2:
    print(f"{names[0]} and {names[1]} like this")
elif likes == 3:
    print(f"{names[0]}, {names[1]} and Mark like this")
else:
    print(f"{names[0]}, {names[1]} and 2 others like this")
