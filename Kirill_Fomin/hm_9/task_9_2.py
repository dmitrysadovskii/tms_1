# Создайте класс вклад. Который содержит необходимые поля и методы,
# например сумма вклада и его срок.
# Пользователь делает вклад в размере N рублей сроком на R лет
# под 10% годовых (вклад с возможностью ежемесячной капитализации - это
# означает, что проценты прибавляются к сумме вклада ежемесячно).
# Написать класс Bank, метод deposit принимает аргументы N и R, и
# возвращает сумму, которая будет на счету пользователя.
#
# https://myfin.by/wiki/term/kapitalizaciya-procentov

class Investment:
    def __init__(self, deposit, term):
        self.deposit = deposit
        self.term = term


class Bank:

    def vklad(self, investment: Investment):
        return round(investment.deposit * (1 + 10 / (12 * 100)
                                           ) ** (investment.term * 12), 2)


dep_1 = Investment(1000, 1)
bsb_1 = Bank()
dep_2 = Investment(1000, 5)
bsb_2 = Bank()
dep_3 = Investment(20000, 2)
bsb_3 = Bank()

print(f'Сумма накоплений в BSB: {bsb_1.vklad(dep_1)}')
print(f'Сумма накоплений в BSB: {bsb_2.vklad(dep_2)}')
print(f'Сумма накоплений в BSB: {bsb_3.vklad(dep_3)}')
