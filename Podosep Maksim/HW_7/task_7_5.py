import string

alphabet_low = string.ascii_lowercase
alphabet_up = string.ascii_uppercase
pun_ion = string.punctuation


def un_code(s, x):

    l_n = len(alphabet_low)
    s1 = ''
    for i in s:
        if i in alphabet_low and i != ' ' and i not in pun_ion:
            a = alphabet_low.index(i) + x
            if a >= l_n:
                b = a - l_n
                s1 = s1 + alphabet_low[b]
            else:
                s1 = s1 + alphabet_low[a]
        elif i in alphabet_up and i != ' ' and i not in pun_ion:
            a = alphabet_up.index(i) + x
            if a >= l_n:
                b = a - l_n
                s1 = s1 + alphabet_up[b]
            else:
                s1 = s1 + alphabet_up[a]
        else:
            s1 = s1 + i
    return s1


def decode(s_dec, x):

    l_n = len(alphabet_low)
    s1 = ''
    for i in s_dec:
        if i in alphabet_low and i != ' ' and i not in pun_ion:
            a = alphabet_low.index(i) - x
            if a < 0:
                b = a + l_n
                s1 = s1 + alphabet_low[b]
            else:
                s1 = s1 + alphabet_low[a]
        elif i in alphabet_up and i != ' ' and i not in pun_ion:
            a = alphabet_up.index(i) - x
            if a < 0:
                b = a + l_n
                s1 = s1 + alphabet_up[b]
            else:
                s1 = s1 + alphabet_up[a]
        else:
            s1 = s1 + i
    return s1


print(decode('Ymnx nx f yjxy xywnsl!', 5))
print(un_code('This is a test string!', 5))
