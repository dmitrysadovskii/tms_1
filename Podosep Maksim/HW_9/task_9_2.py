class Investments:
    def __init__(self, customer, money, term):
        self.customer = customer
        self.money = money
        self.term = term


class Bank:

    def __init__(self, money, term):
        self.money = money
        self.term = term


def invest(investment):
    print(f'{investment.customer} made a investment '
          f'for {investment.term} year(s) in the amount '
          f'of {investment.money} $')


def deposit(investment):
    month = investment.term * 12
    for i in range(month):
        sum_m = (
                (investment.money * (1 + 10 / 100) - investment.money) / 12
        )
        investment.money = investment.money + sum_m

    print(investment.money)


user = 'Max'
n = 1000
r = 1

invest1 = Investments(user, n, r)
investment1 = Bank(n, r)

invest(invest1)
deposit(investment1)
