class Flowers:
    def __init__(self, name, cost, color, freshness, length):
        self.name = name
        self.cost = cost
        self.color = color
        self.freshness = freshness
        self.length = length


class Accessories:
    def __init__(self, name, cost):
        self.name = name
        self.cost = cost


class Bouquet:
    def __init__(self):
        self.flowers = []
        self.accessories = []

    def add_flowers(self, *bouquet):
        for i in bouquet:
            self.flowers.append(i)

    def add_accessories(self, *accessories):
        for i in accessories:
            self.accessories.append(i)

    def cost(self):
        c = 0
        for flower in self.flowers:
            c = c + int(flower.cost)
        for accessory in self.accessories:
            c = c + int(accessory.cost)
        print(f'bouquet cost {c} USD')

    def freshness(self):
        f = 0
        for i in self.flowers:
            f = f + int(i.freshness)
        f_m = f / len(self.flowers)
        print(f'average lifetime of a bouquet {f_m} days')

    def sorting(self, parameter):
        a = []
        b = []
        p = 0
        if parameter == 'name':
            for i in self.flowers:
                a.append(i.name)
                p = 1
        elif parameter == 'cost':
            for i in self.flowers:
                a.append(int(i.cost))
                p = 2
        elif parameter == 'color':
            for i in self.flowers:
                a.append(i.color)
                p = 3
        elif parameter == 'freshness':
            for i in self.flowers:
                a.append(i.freshness)
                p = 4
        elif parameter == 'length':
            for i in self.flowers:
                a.append(i.length)
                p = 5

        a.sort()
        for jj in a:
            for ii in self.flowers:
                if p == 1 and jj == ii.name:
                    b.append(ii.name)
                elif p == 2 and jj == int(ii.cost):
                    b.append(ii.name)
                elif p == 3 and jj == ii.color:
                    b.append(ii.name)
                elif p == 4 and jj == int(ii.freshness):
                    b.append(ii.name)
                elif p == 5 and jj == int(ii.length):
                    b.append(ii.name)
        print(b)

    def search_by_parameter(self, parameter, characteristic):
        if parameter == 'name':
            for i in self.flowers:
                if characteristic == i.name:
                    print(f'this parameter for a flower {i.name}')
        elif parameter == 'cost':
            for i in self.flowers:
                if characteristic == i.cost:
                    print(f'this parameter for a flower {i.name}')
        elif parameter == 'color':
            for i in self.flowers:
                if characteristic == i.color:
                    print(f'this parameter for a flower {i.name}')
        elif parameter == 'freshness':
            for i in self.flowers:
                if characteristic == i.freshness:
                    print(f'this parameter for a flower {i.name}')
        elif parameter == 'length':
            for i in self.flowers:
                if characteristic == i.length:
                    print(f'this parameter for a flower {i.name}')

    def flower_in_bouquet(self, flower_name):
        for i in self.flowers:
            if flower_name == i.name:
                print(f'{i.name} in the bouquet')
                break
        else:
            print(f'{flower_name} is not in the bouquet')


rose_r = Flowers('rose', '10', 'red', '5', '70')
tulip = Flowers('tulip', '7', 'white', '2', '80')
tape = Accessories('tape', '2')

bouquet_1 = Bouquet()
bouquet_1.add_flowers(rose_r, tulip)
bouquet_1.add_accessories(tape)

bouquet_1.cost()
bouquet_1.freshness()
bouquet_1.sorting('cost')
bouquet_1.search_by_parameter('length', '80')
bouquet_1.flower_in_bouquet('camomile')
