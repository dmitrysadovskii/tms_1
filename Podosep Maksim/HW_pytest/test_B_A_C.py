from test_HW_12.B_and_C import BaC
import pytest


def test_1():
    assert BaC('1234', '1234') == 'you are win!!!', 'check the entered numbers'


def test_2():
    assert BaC('1234', '1256') == 'bull = 2, cow = 0',\
        'check the entered numbers'


@pytest.mark.xfail
def test_3():
    assert BaC('', '1256') == 'bull = 2, cow = 0',\
        'check the entered numbers'


@pytest.mark.xfail
def test_4():
    assert BaC('1234', 'mdrt') == 'bull = 2, cow = 0',\
        'check the entered numbers'
