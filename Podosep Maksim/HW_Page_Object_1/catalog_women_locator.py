from selenium.webdriver.common.by import By


class CatalogWomenPageLocator:

    LOCATOR_PRODUCT_SEARCH = (By.XPATH,
                              '//div[@class="right-block"]'
                              '//a[@title="Faded Short Sleeve T-shirts"]')
