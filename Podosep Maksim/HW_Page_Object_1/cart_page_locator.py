from selenium.webdriver.common.by import By


class CartPageLocator:

    LOCATOR_SHOPPING_CART = (By.ID, 'cart_title')
    LOCATOR_EMPTY_CART = (By.XPATH,
                          '//div[@id="center_column"]'
                          '//p[@class="alert alert-warning"]')
    LOCATOR_ADDED_PRODUCT = (By.LINK_TEXT, 'Faded Short Sleeve T-shirts')
