from selenium.webdriver.common.by import By
import pytest
import random
from string import ascii_lowercase
from time import sleep


class TestForm():

    @pytest.fixture()
    def prepare(self, browser):
        browser.get('https://ultimateqa.com/filling-out-forms/')
        self.name = browser.find_element(
            By.XPATH, '//input[@id="et_pb_contact_name_0"]')
        self.message = browser.find_element(
            By.XPATH, '//textarea[@id="et_pb_contact_message_0"]')
        self.submit = browser.find_element(
            By.XPATH, '//div[@id="et_pb_contact_form_0"]//button')
        self.input_name = ''.join(random.sample(ascii_lowercase, 8))
        self.input_m = ''.join(random.sample(ascii_lowercase, 18))

    def test_empty_name(self, browser, prepare):
        self.message.send_keys(self.input_m)
        self.submit.click()
        assert browser.find_element(By.XPATH,
                                    '//div[@class='
                                    '"et-pb-contact-message"]/p').text == \
               'Please, fill in the following fields:'
        assert browser.find_element(
            By.XPATH, '//div[@class="et-pb-contact-message"]/ul[2]').text == ''
        assert browser.find_element(
            By.XPATH, '//div[@class='
                      '"et-pb-contact-message"]/ul[1]').text == 'Name'

    def test_empty_message(self, browser, prepare):
        self.name.send_keys(self.input_name)
        self.submit.click()
        assert browser.find_element(By.XPATH,
                                    '//div[@class='
                                    '"et-pb-contact-message"]/p').text == \
               'Please, fill in the following fields:'
        assert browser.find_element(
            By.XPATH, '//div[@class='
                      '"et-pb-contact-message"]/ul[1]').text == 'Message'
        assert browser.find_element(
            By.XPATH, '//div[@class='
                      '"et-pb-contact-message"]/ul[2]').text == ''

    # This test will fail because of site issues
    def test_filled_form(self, browser, prepare):
        self.name.send_keys(self.input_name)
        self.message.send_keys(self.input_m)
        self.submit.click()
        sleep(5)
        assert browser.find_element(
            By.XPATH, '//div[@id='
                      '"et_pb_contact_form_0"]//p').text == \
            "Form filled out successfully"
