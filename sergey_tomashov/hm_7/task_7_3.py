def gen(ls: list):
    for i in ls:
        if i > 0:
            yield i


number = [34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7]
for i in gen(number):
    print(i)
