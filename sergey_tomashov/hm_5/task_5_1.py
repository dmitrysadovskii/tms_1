import random

correct_number = ''.join(random.sample('1234567890', 4))
# print(correct_number)

while True:

    cows = 0
    bulls = 0
    no = 0

    user_input = input('Enter 4 digits')
    # validate length 4, and is inout contains only from digits
    if (user_input.isdigit() is False) or (len(user_input) != 4):
        print(f'{user_input} input should be 4 non repeatable digits')
        continue

    # break when user type right answer
    if user_input == correct_number:
        print(f'Congrats! You {user_input} same as {correct_number}')
        break

    for i in user_input:
        # break if some numbers in input repeated. f.e. 1123
        if user_input.count(i) > 1:
            print('Repeatable digits in input')
            break

        if i in correct_number:
            if user_input.index(i) != correct_number.index(i):
                cows += 1
            elif user_input.index(i) == correct_number.index(i):
                bulls += 1

        else:
            no += 1
    if cows > 1:
        print(f'{cows} cows')
    elif cows == 1:
        print(f'{cows} cow')
    if bulls > 1:
        print(f'{bulls} bulls')
    elif bulls == 1:
        print(f'{bulls} bull')
    if no == 4:
        print(f'No {bulls} & {cows}')
