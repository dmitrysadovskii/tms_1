from string import digits


def validate_input(card):
    bool_false = False
    if (len(card) < 13) or (len(card) > 19):
        return bool_false
    for i in card:
        if i not in digits:
            return bool_false


def validate(card):
    final = 0
    card = str(card)
    if validate_input(card):
        return False

    for i, v in enumerate(reversed(card)):
        v = int(v)
        if i % 2 == 0:
            final += v
        elif v >= 5:
            final += v * 2 - 9
        else:
            final += v * 2

    if final % 10 == 0:
        return True
    else:
        return False


print(validate(4222222222222))
