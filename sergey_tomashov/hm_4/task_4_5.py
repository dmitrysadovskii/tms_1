"""
Необходимо их объединить по ключам, а значения ключей поместить в список,
если у одного словаря есть ключ "а", а у другого нету,
то поставить значение None на соответствующую позицию
(1-я позиция для 1-ого словаря, вторая для 2-ого)
ab = {'a': [1, None], 'b': [2, None], 'c': [3, 3], 'd':
    [None, 4], 'e': [None, 5]}
"""
a = {'f': 1, 'b': 2, 'c': 3}
b = {'a': 4, 'c': 4, 'e': 5}

un_keys = []
temp_dict = {}

# generate unique keys list
for k in a.keys():
    if k not in b.keys():
        un_keys.append(k)

# search if key from a dict is in unique key,
# than add [a value, None] to temp dict
for k, v in a.items():
    if k in un_keys:
        temp_dict[k] = [v, None]

for k, v in b.items():
    # search for same keys in a and b dicts,
    # if have same key, assign [a value, b value]
    if k in a.keys():
        temp_dict[k] = [a[k], v]
    # than check that b keys not match to unique keys (a)
    # if True, than add [None, b value] to temp_dict
    elif k not in un_keys:
        temp_dict[k] = [None, v]

print(temp_dict)
