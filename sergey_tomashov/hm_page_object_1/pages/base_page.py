from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:
    def __init__(self, driver):
        self.driver = driver
        self.base_page = 'http://automationpractice.com/index.php'

    def open_base_page(self):
        self.driver.get(self.base_page)

    def find_element(self, locator: tuple, time=10):
        return WebDriverWait(self.driver, time).until(
            EC.presence_of_element_located(locator),
            message=f'Cant find element{locator}'
        )

    def find_elements(self, locator: tuple, time=10):
        return WebDriverWait(self.driver, time).until(
            EC.presence_of_all_elements_located(locator),
            message=f'Cant find element{locator}'
        )

    def switch_to_frame(self, locator: tuple, time=10):
        return WebDriverWait(self.driver, time).until(
            EC.frame_to_be_available_and_switch_to_it(locator),
            message=f'Unable to switch frame {locator}'
        )

    def switch_from_frame(self):
        self.driver.switch_to.default_content()
