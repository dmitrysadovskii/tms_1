from sergey_tomashov.hm_page_object_1.pages.base_page \
    import BasePage
from sergey_tomashov.hm_page_object_1.locators.general_locators \
    import GeneralLocators
import random


class GeneralMethods(BasePage, GeneralLocators):

    def random_product(self):
        products = self.find_elements(self.LOCATOR_PRODUCT)
        rand_product = random.choice(products)
        return rand_product

    def get_product_title_from_quick_preview(self):
        product_title = self.find_element(self.LOCATOR_PRODUCT_TITLE)
        return product_title

    def add_product_to_cart(self):
        self.switch_to_frame(self.LOCATOR_PRODUCT_FRAME)
        add_button = self.find_element(self.LOCATOR_ADD_TO_CART)
        add_button.click()
        added_text = self.find_element(self.LOCATOR_ADDED_CART_TEXT).text
        check_text = "Product successfully added to your shopping cart"
        assert added_text == check_text, f'{added_text} != {check_text}'

    def close_quick_preview(self):
        close = self.find_element(self.LOCATOR_CLOSE_QUICK_PREVIEW)
        close.click()
