import unittest
from count import counter


class TestCount(unittest.TestCase):

    def test_positive_abc(self):
        self.assertEqual(counter('ass  saa'), 'as2 2sa2')

    def test_positive_fig(self):
        self.assertEqual(counter('1233'), '1232')

    def test_positive_none(self):
        self.assertEqual(counter(''), '')

    def test_positive_symb(self):
        self.assertEqual(counter('!@#!@##'), '!@#!@#2')

    @unittest.expectedFailure
    def test_negative(self):
        self.assertEqual(counter('1234'), '11213141')


if __name__ == '__main__':
    unittest.main()
