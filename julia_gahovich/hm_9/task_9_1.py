"""Библиотека
Создайте класс book с именем книги, автором, кол-м страниц, ISBN, флагом,
зарезервирована ли книги или нет. Создайте класс пользователь который может
брать книгу, возвращать, бронировать. Если другой пользователь хочет взять
зарезервированную книгу(или которую уже кто-то читает - надо ему про это
сказать).
"""


class Book:

    def __init__(self, book_name, author, page_cnt,
                 isbn, take_by='', reserve_by=''):
        self.book_name = book_name
        self.author = author
        self.page_cnt = page_cnt
        self.isbn = isbn
        self.take_by = str(take_by)
        self.reserve_by = str(reserve_by)

    def reserve_book(self, user_id):
        self.reserve_by = user_id

    def take_book(self, user_id):
        self.take_by = user_id

    def who_reserved(self):
        return self.reserve_by

    def who_taken(self):
        return self.take_by


class User:
    def __init__(self, user_id):
        self.user_id = user_id

    def take_book(self, book: Book):
        if len(book.who_taken()):
            print('Книгу уже забрали')
        else:
            if len(book.who_reserved()):
                if book.who_reserved() == self.user_id:
                    book.take_book(self.user_id)
                    book.reserve_book('')
                else:
                    print('Извините, книгу забронировали не вы')
            else:
                book.take_book(self.user_id)

    def reserve_book(self, book: Book):
        if not(book.who_reserved()):
            book.reserve_book(self.user_id)
        else:
            if book.who_reserved() != self.user_id:
                print('Извините, книга уже забронирована')
            else:
                print('Вы уже забронировали эту книгу')


book_1 = Book('Война и мир', 'Толстой', '570', '978-5-08-004667-4')
book_2 = Book('Горе от ума', 'Грибоедов', '111', '978-5-699-30769-2')
user_1 = User('Гриша')
user_2 = User('Валера')
user_1.reserve_book(book_1)
user_1.reserve_book(book_1)
user_2.reserve_book(book_1)
user_2.take_book(book_2)
user_1.take_book(book_2)
user_1.take_book(book_1)
