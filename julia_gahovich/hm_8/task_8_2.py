"""На вход подаётся некоторое количество (не больше сотни)разделённых пробелом
целых чисел (каждое не меньше 0 и не больше 19). Выведите их через пробел в
порядке лексикографического возрастания названий этих чисел в английском языке
"""

number_names = {0: 'zero', 1: 'one', 2: 'two', 3: 'three', 4: 'four',
                5: 'five', 6: 'six', 7: 'seven', 8: 'eight', 9: 'nine',
                10: 'ten', 11: 'eleven', 12: 'twelve', 13: 'thirteen',
                14: 'fourteen', 15: 'fifteen', 16: 'sixteen',
                17: 'seventeen', 18: 'eighteen', 19: 'nineteen'}


def decorator(func):
    def wrapper(a):
        b = []
        a = a.split()
        for k, v in number_names.items():
            for i in a:
                if int(i) == k:
                    b.append(v)

        func(' '.join(sorted(b, key=str)))
    return wrapper


@decorator
def one_function(a):
    print(a)


one_function('1 14 12 8 17 4 19 2 0')
