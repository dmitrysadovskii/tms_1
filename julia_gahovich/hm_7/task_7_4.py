"""Генераторы:
Необходимо составить список чисел которые указывают на длину слов в строке:
sent = " thequick brown fox jumps over the lazy dog",
но только если слово не "the".
"""


def sent_len_generation(sent):
    sent = sent.split()
    new_list = [len(x) for x in sent if x != 'the']
    print(new_list)


sent_len_generation(' thequick brown fox jumps over the lazy dog')
