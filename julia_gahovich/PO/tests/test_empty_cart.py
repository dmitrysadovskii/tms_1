"""1. Проверить что корзина пуста
Оформить как два разных теста в стиле PO"""
from pages.main_page import MainPage
from pages.cart_page import CartPage


def test_empty_cart(browser):
    main_page = MainPage(browser)
    main_page.open_base_page()
    main_page.open_cart_page()
    cart_page = CartPage(browser)
    cart_page.should_be_cart_page()
    cart_page.cart_text_is_present()
