from pages.base_page import BasePage
from locator.main_page_locator import MainPageLocator
from locator.cart_page_locator import CartPageLocator


class CartPage(BasePage):

    def open_cart_page(self):
        cart_link = self.find_element(
            MainPageLocator.LOCATOR_CART)

        cart_link.click()

    def should_be_cart_page(self):
        self.cart_text_is_present()

    def cart_text_is_present(self):
        cart_text = self.find_element(CartPageLocator.LOCATOR_CART_TEXT).text
        assert cart_text == 'SHOPPING-CART SUMMARY'

    def empty_cart_msg_is_present(self):
        empty_cart_msg = self.find_element(
            CartPageLocator.LOCATOR_CART_EMPTY_MSG).text
        assert empty_cart_msg == 'Your shopping cart is empty.'

    def product_counter_is_present(self):
        product_counter_text = self.find_element(
            CartPageLocator.LOCATOR_PRODUCTS_COUNTER).text
        assert 'Your shopping cart contains:' in product_counter_text

    def product_details_is_present(self):
        product_details = self.find_element(
            CartPageLocator.LOCATOR_ORDER_DETAIL)
        assert product_details, 'No product table'

    def checkout_button_is_present(self):
        check_button = self.find_element(
            CartPageLocator.LOCATOR_CHECKOUT_BUTTON)
        assert check_button, 'No checkout button'
