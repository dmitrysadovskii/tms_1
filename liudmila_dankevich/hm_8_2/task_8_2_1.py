# 1.Реализуйте декоратор caching, который в течение X последних секунд с
# момента последнего вызова(в нашему случае 3) будет запоминать
# вычесленное ранее значение, и возвращать его вместо повторного
# вызова функции.Если время таймаута истекло между вызовами функции, значение
# должно быть вычислено заново и еще раз закешировано на X секунд.


from time import time, sleep


def caching(timeout):
    cache = {}

    def wrapper(func):
        def inner(x):
            res = time()
            if x in cache.keys():
                if (res - cache[x]) > timeout:
                    cache[func(x)] = res
                    print(cache)
                else:
                    print()
            else:
                cache[func(x)] = res
                print(cache)
        return inner

    return wrapper


@caching(timeout=3)
def func(x):
    return x


func(5)
sleep(3)
func(4)
sleep(2)
func(3)
