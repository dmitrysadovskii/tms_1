# Цветочница.
# Определить иерархию и создать несколько цветов. Собрать букет
# (можно использовать аксессуары) с определением его стоимости.
# Определить время его увядания по среднему времени жизни всех цветов в букете.
# Позволить сортировку цветов в букете на основе различных параметров
# (свежесть/цвет/длина стебля/стоимость...)
# Реализовать поиск цветов в букете по определенным параметрам.
# Узнать, есть ли цветок в букете.

from abc import ABC, abstractmethod


class Flower(ABC):

    @abstractmethod
    def __init__(self, fl_name: str, fl_price: int,
                 fl_length: int, fl_lifetime: int):
        self.fl_name = fl_name
        self.fl_price = fl_price
        self.fl_length = fl_length
        self.fl_lifetime = fl_lifetime


class Rose(Flower):
    def __init__(self):
        super().__init__('Rose', 15, 6, 5)


class Peony(Flower):
    def __init__(self):
        super().__init__('Peony', 12, 2, 4)


class Iris(Flower):
    def __init__(self):
        super().__init__('Iris', 18, 2, 8)


class Bouquet_of_flowers:
    flowers = []

    def add_flower(self, flower: Flower):
        self.flowers.append(flower)

    def cost(self):
        cost = 0
        for flower in self.flowers:
            cost += flower.fl_price
        return cost

    def lifetime(self):
        avg_lft = 0
        for flower in self.flowers:
            avg_lft += flower.fl_lifetime

        if len(self.flowers) > 0:
            return avg_lft / len(self.flowers)
        else:
            return 0

    def show_flowers(self):
        for flower in self.flowers:
            print('name: ' + flower.fl_name + ', price: ' + str(
                flower.fl_price) + ', length: ' + str(
                flower.fl_length) + ', lifetime: ' + str(flower.fl_lifetime))

    def sort(self, field):
        self.flowers.sort(key=lambda x: getattr(x, field))

    def find_flower(self, key, value):
        result = []
        for i in self.flowers:
            if getattr(i, key) == value:
                result.append(i)
        for flower in result:
            print('name: ' + flower.fl_name)


rose_1 = Rose()
rose_2 = Rose()
peony = Peony()
iris_1 = Iris()
iris_2 = Iris()

bouquet = Bouquet_of_flowers()

bouquet.add_flower(rose_1)
bouquet.add_flower(rose_1)
bouquet.add_flower(peony)
bouquet.add_flower(iris_1)
bouquet.add_flower(iris_2)

bouquet.sort('fl_length')
bouquet.show_flowers()

bouquet.find_flower('fl_length', 6)
bouquet.find_flower('fl_name', 'iris')
