# Необходимо составить список чисел которые указывают
# на длину слов в строке: sentence =
# " thequick brown fox jumps over the lazy dog",
# но только если слово не "the"
# from typing import List

sentence = " thequick brown fox jumps over the lazy dog"


def str_count(sentence_1: str):
    sentence_1 = sentence_1.split()
    for i in sentence_1:
        if i != 'the':
            yield len(i)


def list_1(sentence_1: str) -> list:
    res = []
    for i in str_count(sentence_1):
        res.append(i)
    return res


print(list_1(sentence))
