# Like
# Создайте программу, которая, принимая массив имён,
# возвращает строку описывающая
# количество лайков (как в Facebook).

# Примеры:
# likes() -> "no one likes this"
# likes("Ann") -> "Ann likes this"
# likes("Ann", "Alex") -> "Ann and Alex like this"
# likes("Ann", "Alex", "Mark") -> "Ann, Alex and Mark like this"
# likes("Ann", "Alex", "Mark", "Max") -> "Ann, Alex and 2 others like this"


likes = ['Dmitriy', 'Any', 'Sergey', 'Any', 'Ira', 'Sergey', 'Oly', 'Sasha']
l_lik = len(likes)

if l_lik > 3:
    print(f'{likes[0]}, {likes[1]} и {l_lik - 2} other like this')
elif l_lik == 3:
    print(f'{likes[0]}, {likes[1]} и {likes[2]} like this')
elif l_lik == 2:
    print(f'{likes[0]} и {likes[1]} like this')
elif l_lik == 1:
    print(f'{likes[0]} like this')
else:
    print("no one like this")
