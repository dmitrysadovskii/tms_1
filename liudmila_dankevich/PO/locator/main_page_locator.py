from selenium.webdriver.common.by import By


class MainPageLocator:

    LOCATOR_SING_IN_BUTTON = (By.CLASS_NAME, 'login')
    LOCATOR_CART_BUTTON = (By.XPATH,
                           '//div[@class="shopping_cart"]/a'
                           )
    LOCATOR_WOMEN_BUTTON = (By.CLASS_NAME, 'sf-with-ul')
