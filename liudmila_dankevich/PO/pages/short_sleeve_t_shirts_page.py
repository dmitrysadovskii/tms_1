from pages.base_page import BasePage
from locator.short_sleeve_t_shirts_locator import ShortSleeveshirtsLocator


class ShortSleeveshirtsPage(BasePage):
    # добавляем продукт в карзину

    def product_add_to_cart(self):
        cart_button = self.find_element(
            ShortSleeveshirtsLocator.LOCATOR_ADD_TO_CART)
        cart_button.click()

    # открываем карзину

    def open_cart(self):
        button_open_cart = self.find_element(
            ShortSleeveshirtsLocator.LOCATOR_CART)
        button_open_cart.click()

    def proceed_to_checkout(self):
        button_proceed_to_checkout = self.find_element(
            ShortSleeveshirtsLocator.LOCATOR_PROCEED_TO_CHECKOUT)
        button_proceed_to_checkout.click()
